php bin/console assets:install --symlink
composer install --prefer-dist --no-progress --no-suggest --no-interaction
chown -R www-data:www-data *
chown  www-data:www-data /etc/yubico/keys.json
cd web
yarn install ; yarn run dev
