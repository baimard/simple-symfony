FROM php:7.4-fpm-buster

COPY entrypoint.sh /entrypoint.sh
COPY generate_keys_bdd.py /etc/yubico/generate_keys_bdd.py
COPY decrypt_aead_bdd.py /etc/yubico/decrypt_aead_bdd.py

# Install
RUN apt-get update -q && \
    apt upgrade -y && \
    apt-get install -qq -y \
    curl wget gnupg2 bash git nodejs \
    npm python2 python-pip python-pymysql \
    default-mysql-client libmariadbclient-dev libzip-dev \
    chromium-driver chromium zip unzip

# Yarn
RUN curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
RUN echo "deb https://dl.yarnpkg.com/debian/ stable main" |  tee /etc/apt/sources.list.d/yarn.list
RUN apt-get update && apt-get upgrade -y && apt-get install -y  yarn

# Clean
RUN apt-get autoremove --purge && apt-get clean

# Pip install
RUN pip install mysqlclient pyhsm sqlalchemy

# Install pdo
RUN docker-php-ext-install pdo_mysql sockets zip

# Install composer
RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" && \
    php composer-setup.php && php -r "unlink('composer-setup.php');" && \
    mv composer.phar /usr/local/bin/composer

# Install dépendance tests
RUN composer req symfony/panther
RUN composer req --dev symfony/panther

# Install dépendance
RUN yarn --version

RUN yarn add --dev @symfony/webpack-encore --dev && \
    yarn add @symfony/stimulus-bridge@^2.0.0 stimulus --dev \
    yarn add webpack-notifier@^1.6.0 --dev

# Symfony CLI
RUN wget https://get.symfony.com/cli/installer -O - | bash && mv /root/.symfony/bin/symfony /usr/local/bin/symfony

WORKDIR /var/www/html